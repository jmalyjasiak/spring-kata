package com.kata.family.query.model.dto;

import com.kata.child.query.dto.ChildDto;
import com.kata.father.query.dto.FatherDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FamilyDto {

    private int id;
    private FatherDto father;
    private Set<ChildDto> children;
}
