package com.kata.family.query;

import com.kata.family.query.exception.FamilyNotFoundException;
import com.kata.utils.exception.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class FamilyQueryControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(FamilyNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody ErrorDto handleFamilyNotFound(FamilyNotFoundException ex) {
        return ErrorDto.ofException(ex);
    }
}
