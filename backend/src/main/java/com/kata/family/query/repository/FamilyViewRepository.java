package com.kata.family.query.repository;

import com.kata.family.query.model.view.FamilyViewDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FamilyViewRepository extends JpaRepository<FamilyViewDto, Integer> {
}
