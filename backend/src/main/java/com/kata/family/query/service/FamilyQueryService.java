package com.kata.family.query.service;

import com.kata.family.query.model.dto.FamilyDto;
import com.kata.family.query.model.view.FamilyViewDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FamilyQueryService {

    Page<FamilyViewDto> findAllFamilyView(Pageable pageable);
    FamilyDto findOneById(int id);
}
