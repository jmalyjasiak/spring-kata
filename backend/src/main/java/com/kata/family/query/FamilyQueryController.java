package com.kata.family.query;

import com.kata.family.query.model.dto.FamilyDto;
import com.kata.family.query.model.view.FamilyViewDto;
import com.kata.family.query.service.FamilyQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/families")
@RequiredArgsConstructor
public class FamilyQueryController {

    private final FamilyQueryService familyQueryService;

    @GetMapping
    public Page<FamilyViewDto> findAll(Pageable pageable) {
        return familyQueryService.findAllFamilyView(pageable);
    }

    @GetMapping("/{id}")
    public FamilyDto findOneById(@PathVariable int id) {
        return familyQueryService.findOneById(id);
    }
}
