package com.kata.family.query.repository;

import com.kata.family.model.entity.Family;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyQueryRepository extends JpaRepository<Family, Integer> {
}
