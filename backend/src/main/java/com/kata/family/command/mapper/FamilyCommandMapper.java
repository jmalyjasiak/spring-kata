package com.kata.family.command.mapper;

import com.kata.family.command.dto.FamilyIdDto;
import com.kata.family.model.entity.Family;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface FamilyCommandMapper {

    FamilyIdDto toFamilyCreatedDto(Family family);
    Family toFamily(FamilyIdDto familyIdDto);
}
