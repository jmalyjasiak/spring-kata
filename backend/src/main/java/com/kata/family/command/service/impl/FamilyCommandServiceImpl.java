package com.kata.family.command.service.impl;

import com.kata.family.command.dto.FamilyIdDto;
import com.kata.family.command.mapper.FamilyCommandMapper;
import com.kata.family.command.service.FamilyCommandService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FamilyCommandServiceImpl implements FamilyCommandService {

    private final FamilyCreator familyCreator;
    private final FamilyCommandMapper familyCommandMapper;

    @Override
    public FamilyIdDto createFamily() {
        var family = familyCreator.createFamily();
        return familyCommandMapper.toFamilyCreatedDto(family);
    }
}
