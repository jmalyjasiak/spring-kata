package com.kata.family.command.service;

import com.kata.family.command.dto.FamilyIdDto;

public interface FamilyCommandService {

    FamilyIdDto createFamily();
}
