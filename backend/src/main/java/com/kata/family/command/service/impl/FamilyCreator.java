package com.kata.family.command.service.impl;

import com.kata.family.command.repository.FamilyCommandRepository;
import com.kata.family.model.entity.Family;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class FamilyCreator {

    private final FamilyCommandRepository familyCommandRepository;

    Family createFamily() {
        return familyCommandRepository.save(new Family());
    }
}
