package com.kata.family.command.repository;

import com.kata.family.model.entity.Family;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FamilyCommandRepository extends JpaRepository<Family, Integer> {

}
