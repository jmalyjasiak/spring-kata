package com.kata.family.model.entity;

import com.kata.child.model.entity.Child;
import com.kata.father.model.entity.Father;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"father", "children"})
@ToString(exclude = {"father", "children"})
public class Family {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "father_id")
    private Father father;

    @OneToMany(mappedBy = "family")
    private Set<Child> children;
}
