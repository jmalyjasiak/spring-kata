package com.kata.father.command.repository;

import com.kata.father.model.entity.Father;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FatherCommandRepository extends JpaRepository<Father, Integer> {
}
