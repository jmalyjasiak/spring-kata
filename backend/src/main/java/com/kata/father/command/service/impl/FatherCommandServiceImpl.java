package com.kata.father.command.service.impl;

import com.kata.family.query.service.FamilyQueryService;
import com.kata.father.command.dto.CreateFatherDto;
import com.kata.father.command.dto.FatherIdDto;
import com.kata.father.command.exception.FatherCreationException;
import com.kata.father.command.mapper.FatherCommandMapper;
import com.kata.father.command.service.FatherCommandService;
import com.kata.father.command.validator.FatherValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FatherCommandServiceImpl implements FatherCommandService {

    private final FatherCreator fatherCreator;
    private final FatherCommandMapper fatherCommandMapper;
    private final FamilyQueryService familyQueryService;

    @Override
    public FatherIdDto addFatherToFamily(CreateFatherDto createFatherDto) {
        var father = fatherCommandMapper.toFather(createFatherDto);
        var familyDto = familyQueryService.findOneById(father.getFamily().getId());

        FatherValidator.of(father, familyDto)
                .isFamilyPresent()
                .isFatherAlreadyPresent()
                .ifInvalidThrow(FatherCreationException::new);

        var createdFather = fatherCreator.createFather(father);
        return fatherCommandMapper.toFatherIdDto(createdFather);
    }
}
