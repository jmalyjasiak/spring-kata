package com.kata.father.command.validator;

import com.kata.family.query.model.dto.FamilyDto;
import com.kata.father.model.entity.Father;
import lombok.RequiredArgsConstructor;

import java.util.function.Supplier;

@RequiredArgsConstructor(staticName = "of")
public class FatherValidator {

    private final Father father;
    private final FamilyDto familyDto;
    private boolean isValid = true;

    public FatherValidator isFamilyPresent() {
        if(!this.isValid || this.familyDto == null) {
            this.isValid = false;
        }
        return this;
    }

    public FatherValidator isFatherAlreadyPresent() {
        if(!this.isValid || this.familyDto.getFather() != null) {
            this.isValid = false;
        }
        return this;
    }

    public void ifInvalidThrow(Supplier<RuntimeException> runtimeExceptionSupplier) {
        if(!this.isValid) {
            throw runtimeExceptionSupplier.get();
        }
    }
}
