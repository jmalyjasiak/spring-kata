package com.kata.father.command.mapper;

import com.kata.family.command.mapper.FamilyCommandMapper;
import com.kata.family.query.mapper.FamilyQueryMapper;
import com.kata.father.command.dto.CreateFatherDto;
import com.kata.father.command.dto.FatherIdDto;
import com.kata.father.model.entity.Father;
import org.mapstruct.Mapper;

@Mapper(uses = {FamilyQueryMapper.class, FamilyCommandMapper.class})
public interface FatherCommandMapper {

    FatherIdDto toFatherIdDto(Father father);
    Father toFather(CreateFatherDto father);
}
