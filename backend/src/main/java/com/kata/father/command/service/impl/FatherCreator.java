package com.kata.father.command.service.impl;

import com.kata.father.command.repository.FatherCommandRepository;
import com.kata.father.model.entity.Father;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class FatherCreator {

    private final FatherCommandRepository fatherCommandRepository;

    Father createFather(Father father) {
        return fatherCommandRepository.save(father);
    }
}
