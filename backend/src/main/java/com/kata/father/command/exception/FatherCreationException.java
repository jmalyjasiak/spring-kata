package com.kata.father.command.exception;

public class FatherCreationException extends RuntimeException {
    private final static String DEFAULT_MESSAGE = "Could not create father";

    public FatherCreationException() {
        super(DEFAULT_MESSAGE);
    }

    public FatherCreationException(Throwable t) {
        super(DEFAULT_MESSAGE, t);
    }
}
