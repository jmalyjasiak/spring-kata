package com.kata.father.model.entity;

import com.kata.family.model.entity.Family;
import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(exclude = "")
public class Father {

    @Id
    @GeneratedValue
    private Integer id;
    private String firstName;
    private String secondName;
    private String pesel;
    private ZonedDateTime birthDate;

    @OneToOne(mappedBy = "father", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Family family;
}
