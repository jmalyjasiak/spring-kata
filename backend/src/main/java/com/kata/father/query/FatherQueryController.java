package com.kata.father.query;

import com.kata.father.query.dto.FatherDto;
import com.kata.father.query.service.FatherQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fathers")
@RequiredArgsConstructor
public class FatherQueryController {

    private final FatherQueryService fatherQueryService;

    @GetMapping("/{id}")
    public FatherDto getFatherById(@PathVariable Integer id) {
        return fatherQueryService.findOneById(id);
    }

}
