package com.kata.father.query.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FatherDto {

    private int id;
    private String firstName;
    private String secondName;
    private String pesel;
    private ZonedDateTime birthDate;
}
