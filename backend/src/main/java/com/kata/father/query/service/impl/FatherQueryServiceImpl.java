package com.kata.father.query.service.impl;

import com.kata.father.query.dto.FatherDto;
import com.kata.father.query.exception.FatherNotFoundException;
import com.kata.father.query.mapper.FatherQueryMapper;
import com.kata.father.query.repository.FatherQueryRepository;
import com.kata.father.query.service.FatherQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FatherQueryServiceImpl implements FatherQueryService {

    private final FatherQueryRepository fatherQueryRepository;
    private final FatherQueryMapper fatherQueryMapper;

    @Override
    public FatherDto findOneById(int id) {
        var father = fatherQueryRepository.findById(id).orElseThrow(FatherNotFoundException::new);
        return fatherQueryMapper.toFatherDto(father);
    }
}
