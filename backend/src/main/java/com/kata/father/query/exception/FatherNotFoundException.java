package com.kata.father.query.exception;

public class FatherNotFoundException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Could not find father";

    public FatherNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public FatherNotFoundException(Throwable t) {
        super(DEFAULT_MESSAGE, t);
    }
}
