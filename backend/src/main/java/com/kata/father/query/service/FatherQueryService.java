package com.kata.father.query.service;

import com.kata.father.query.dto.FatherDto;

public interface FatherQueryService {

    FatherDto findOneById(int id);
}
