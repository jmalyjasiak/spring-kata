package com.kata.father.query;

import com.kata.father.command.exception.FatherCreationException;
import com.kata.father.query.exception.FatherNotFoundException;
import com.kata.utils.exception.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class FatherQueryControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(FatherNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorDto handleFatherCreationError(FatherNotFoundException ex) {
        return ErrorDto.ofException(ex);
    }
}
