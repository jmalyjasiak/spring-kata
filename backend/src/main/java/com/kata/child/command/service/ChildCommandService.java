package com.kata.child.command.service;

import com.kata.child.command.dto.ChildIdDto;
import com.kata.child.command.dto.CreateChildDto;

public interface ChildCommandService {

    ChildIdDto createChild(CreateChildDto createChildDto);
}
