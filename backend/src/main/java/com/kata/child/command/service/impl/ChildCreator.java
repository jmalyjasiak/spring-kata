package com.kata.child.command.service.impl;

import com.kata.child.command.repository.ChildCommandRepository;
import com.kata.child.model.entity.Child;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class ChildCreator {

    private final ChildCommandRepository childCommandRepository;

    Child createChild(Child child) {
        return childCommandRepository.save(child);
    }
}
