package com.kata.child.command.dto;

import com.kata.child.model.enumeration.Sex;
import com.kata.family.command.dto.FamilyIdDto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class CreateChildDto {

    @NotNull
    private FamilyIdDto family;
    @NotBlank
    private String firstName;
    private String secondName;
    @Size(min = 11, max = 11)
    @Pattern(regexp = "[0-9]+")
    private String pesel;
    @NotNull
    private Sex sex;
}
