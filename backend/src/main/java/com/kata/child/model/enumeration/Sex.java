package com.kata.child.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Sex {
    M("Male"),
    F("Female");

    private String fullName;
}
