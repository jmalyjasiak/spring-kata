package com.kata.child.model.entity;


import com.kata.child.model.enumeration.Sex;
import com.kata.family.model.entity.Family;
import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Child {

    @Id
    @GeneratedValue
    private Integer id;
    private String firstName;
    private String pesel;
    private String secondName;
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @ManyToOne
    @JoinColumn(name = "family_id")
    private Family family;
}
