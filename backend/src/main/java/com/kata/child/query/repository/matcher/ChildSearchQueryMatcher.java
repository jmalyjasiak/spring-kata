package com.kata.child.query.repository.matcher;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.ExampleMatcher;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;

@Configuration
public class ChildSearchQueryMatcher {

    @Bean
    public ExampleMatcher ChildSearchExampleMatcher() {
        return ExampleMatcher.matching()
                .withMatcher("firstName", startsWith().ignoreCase())
                .withMatcher("secondName", startsWith().ignoreCase());
    }
}
