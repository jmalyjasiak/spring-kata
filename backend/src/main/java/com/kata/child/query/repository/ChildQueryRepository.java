package com.kata.child.query.repository;

import com.kata.child.model.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildQueryRepository extends JpaRepository<Child, Integer> {
}
