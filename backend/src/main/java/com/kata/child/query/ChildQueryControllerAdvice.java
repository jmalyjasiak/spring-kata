package com.kata.child.query;

import com.kata.child.query.exception.ChildNotFoundException;
import com.kata.utils.exception.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ChildQueryControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ChildNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorDto handleChildNotFoundException(ChildNotFoundException ex) {
        return ErrorDto.ofException(ex);
    }
}
