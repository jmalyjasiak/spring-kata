package com.kata.child.query.service.impl;

import com.kata.child.model.entity.Child;
import com.kata.child.query.dto.ChildDto;
import com.kata.child.query.dto.ChildSearchDto;
import com.kata.child.query.exception.ChildNotFoundException;
import com.kata.child.query.mapper.ChildQueryMapper;
import com.kata.child.query.repository.ChildQueryRepository;
import com.kata.child.query.service.ChildQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChildQueryServiceImpl implements ChildQueryService {

    private final ChildQueryMapper childQueryMapper;
    private final ExampleMatcher childExampleMatcher;
    private final ChildQueryRepository childQueryRepository;

    @Override
    public ChildDto findOneById(int id) {
        return childQueryRepository.findById(id)
                .map(childQueryMapper::toChildDto)
                .orElseThrow(ChildNotFoundException::new);
    }

    @Override
    public Page<ChildDto> searchChild(ChildSearchDto childSearchDto, Pageable pageable) {
        var child = childQueryMapper.toChild(childSearchDto);
        var childExample = Example.of(child, childExampleMatcher);
        Page<Child> childrenFound = childQueryRepository.findAll(childExample, pageable);
        return childQueryMapper.toChildDtoPage(childrenFound);
    }
}
