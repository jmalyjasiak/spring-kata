package com.kata.child.query.exception;

public class ChildNotFoundException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Could not find child";

    public ChildNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public ChildNotFoundException(Throwable t) {
        super(DEFAULT_MESSAGE, t);
    }
}
