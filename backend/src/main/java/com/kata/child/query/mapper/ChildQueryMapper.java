package com.kata.child.query.mapper;

import com.kata.child.model.entity.Child;
import com.kata.child.query.dto.ChildDto;
import com.kata.child.query.dto.ChildSearchDto;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

@Mapper
public interface ChildQueryMapper {

    ChildDto toChildDto(Child child);
    Child toChild(ChildSearchDto childSearchDto);

    default Page<ChildDto> toChildDtoPage(Page<Child> childrenFound) {
        return childrenFound.map(this::toChildDto);
    }
}
