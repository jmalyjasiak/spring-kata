package com.kata.utils.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ErrorDto {

    private String message;

    public static ErrorDto ofException(Exception ex) {
        return new ErrorDto(ex.getMessage());
    }
}
