package com.kata.utils.datetime;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.util.Objects.isNull;

public class DateTimeMapper {
    public static ZonedDateTime toZonedDateTime(Timestamp timestamp) {
        var withoutTimezone = toLocalDateTime(timestamp);
        return isNull(withoutTimezone) ? null : withoutTimezone.atZone(ZoneId.systemDefault());
    }

    public static LocalDateTime toLocalDateTime(Timestamp timestamp) {
        return isNull(timestamp) ? null : timestamp.toLocalDateTime();
    }
}
