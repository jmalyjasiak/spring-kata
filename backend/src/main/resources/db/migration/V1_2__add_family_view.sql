CREATE VIEW family_father_children as
  SELECT FY.id, FR.id as father_id, FR.first_name as father_first_name, FR.second_name as father_second_name, FR.pesel as father_pesel, FR.birth_date as father_birth_date,
    COUNT(CD.id) as children_count
  FROM FAMILY FY
    LEFT JOIN FATHER FR on FY.father_id = FR.id
    LEFT JOIN CHILD CD on CD.family_id = FY.id
  GROUP BY FY.ID, FR.id, FR.first_name, FR.second_name, FR.pesel, FR.birth_date;