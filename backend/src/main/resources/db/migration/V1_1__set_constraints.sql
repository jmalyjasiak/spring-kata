ALTER TABLE FAMILY
    ADD CONSTRAINT fk_father_id_family_father_id FOREIGN KEY (father_id) REFERENCES FATHER (id) ON DELETE CASCADE;

ALTER TABLE CHILD
    ADD CONSTRAINT fk_family_if_child_family_id FOREIGN KEY (family_id) REFERENCES FAMILY (id);