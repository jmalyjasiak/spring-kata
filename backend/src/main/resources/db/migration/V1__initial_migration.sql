CREATE TABLE FATHER (
  id INTEGER PRIMARY KEY NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  second_name VARCHAR(100),
  pesel VARCHAR(11) NOT NULL,
  birth_date TIMESTAMP NOT NULL
);

CREATE TABLE FAMILY (
  id INTEGER PRIMARY KEY NOT NULL,
  father_id INTEGER
);

CREATE TABLE CHILD (
  id INTEGER PRIMARY KEY NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  second_name VARCHAR(100),
  pesel VARCHAR(11) NOT NULL,
  sex VARCHAR(100) NOT NULL,
  family_id INTEGER NOT NULL
);