##  Spring and Angular training project

### Backend

#### Requirements 
1. Java 10

#### Guide
Run postgres db prior to backend app. Config is inside `docker-compose.yml` file

To run backend manually go for `./mvnw spring-boot:run` in ./backend.

Swagger is enabled and one can find it on <app-url>/swagger-ui.html. app-url wil be localhost:8080 most probably.

### Frontend

#### Requirements
1. Node 10.x 
2. npm

To run it manualy type npm install && npm start in ./frontend directory.

### Running entire stack application

#### Requirements
1. Docker 
2. Docker-compose

#### Steps
Everything has been dockerized, so one can run entire architecture using `docker-compose up` in projects root directory.
